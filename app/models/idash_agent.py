from app.idash import idash

# cache for idash
adgroup_infos = {}


def get_info_by_cid_from_idash(cid):
    if cid not in adgroup_infos:
        print(f'load {cid} info from idash')
        adgroup = idash.adgroup(cid=cid, fields=['name', 'oid'])
        adgroup_name = adgroup['name']
        oid = adgroup['oid']
        cm_name = idash.campaign(oid=oid, fields=['order_managers'])['order_managers'][0].split('@')[0]
        if cm_name is None:
            cm_name = 'Appier CM team'
        adgroup_infos[cid] = (oid, adgroup_name, cm_name)

    return adgroup_infos[cid]
