import json


def load(mailing_list_path):
    with open(mailing_list_path, 'r') as f:
        mailing_list = json.load(f)

    return mailing_list
