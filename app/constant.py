import pathlib
import yaml

from app.models import mailing_list_loader

config_path = f'{pathlib.PurePath(__file__).parent.parent}/config.yml'
with open(config_path, 'r') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

mailing_list_dict = mailing_list_loader.load(config['app']['mailing_list_path'])

_BASIC_COLUMNS = [
    ('APP / Site ID', 'channel id'),
    ('Clicks', 'clicks'),
    ('Actions', 'installs'),
    ('Cost', 'cost'),
    ('CVR', 'cvr'),
    ('CPA', 'cpa'),
    ('1DRR', '1drr'),
    ('Deduplicated Actions', 'installs_dedupe')
]
_PUB_COLUMNS = _BASIC_COLUMNS + [
    ('7D APP PURCHASE', '7d app purchase')
]
_SUB_COLUMNS = _BASIC_COLUMNS + [
    ('3D APP REGISTER', '3d app register'),
    ('3D APP TUTORIAL', '3d app tutorial'),
    ('3D LOGIN', '3d login'),
    ('3D App Character', '3d app character'),
    ('3D App Levelup', '3d app levelup'),
    ('7D APP PURCHASE', '7d app purchase')
]
OUTPUT_COLUMNS_DICT = {
    'pub': _PUB_COLUMNS,
    'sub': _SUB_COLUMNS,
    'basic': _BASIC_COLUMNS
}

EXCLUDED_PARTNER = {
    'personaly'
}

SUMMARY_PARTNER = {
    'doglobal',
    'doglobal_api',
    'newborntown',
    'creativeclicks',
    'dotc'
}
