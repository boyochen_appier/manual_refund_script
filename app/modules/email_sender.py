import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from os.path import basename

from app.modules.email_generator import generate_mail_attachment, generate_mail_text,\
    generate_campaign_level_mail_attachment, generate_campaign_level_mail_text
from app.modules import log_recorder
from app.constant import config, mailing_list_dict, EXCLUDED_PARTNER, SUMMARY_PARTNER


def send_mail_util(receiver_list, subject, mail_body, image, file, retry_count=10, mime_type='html', sender='cm_adops@appier.com'):
    body = MIMEMultipart()
    body['From'] = sender
    body['To'] = ','.join(receiver_list)
    body['Subject'] = subject

    html_mail_body = mail_body.replace('\n', '<br>')
    mail_content = f'<html><body><p>{html_mail_body}</p>'
    mail_content += '<img src="cid:screenshot" />' if image else ''
    mail_content += '</body><html>'
    body.attach(MIMEText(mail_content, mime_type, 'utf-8'))
    if file:
        with open(file, "rb") as fd:
            part = MIMEApplication(
                fd.read(),
                Name=basename(file)
            )
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(file)
            body.attach(part)

    if image:
        with open(image, 'rb') as fd:
            part = MIMEImage(
                fd.read(),
                name=basename(image)
            )
            part['content-Id'] = '<screenshot>'
            body.attach(part)

    while retry_count > 0:
        try:
            retry_count -= 1
            smtp_obj = smtplib.SMTP(host='email-smtp.us-east-1.amazonaws.com', port=587)
            smtp_obj.starttls()
            smtp_obj.login('AKIAIH2TIHJPPFRFAXJQ', 'Ai0s/dM9GxVxjG8quGpBlyyUQazmXdlvVAmVSBPTybMB')
            smtp_obj.sendmail(sender, receiver_list, body.as_string())
            print(f"Successfully sent email: {subject}")
            return True
        except smtplib.SMTPException:
            traceback.print_exc()
            print("Unable to send email")
    return False


def batch_send_mail(data_df, target_inv_level, testing=config['testing'], log_only=config['log_only']):
    for i, info_series in data_df.iterrows():

        print(
            '%d: try to send mail: partner: %s, cid: %s, inv: %s' % (
                i, info_series['partner id'], info_series['cid'], info_series['channel id']
            )
        )

        if info_series['cost'] in ['#VALUE!', '0']:
            print('error: weird cost: %s' % info_series['cost'])
            continue

        if info_series['partner id'] in EXCLUDED_PARTNER:
            print('Partner: %s, skipped!' % info_series['partner id'])
            continue

        subject, mail_body = generate_mail_text(info_series)

        if log_only:
            log_recorder.record(info_series, subject)
        else:
            receiver_list = ['jim.chang@appier.com'] if testing \
                else mailing_list_dict[info_series['partner id']] + ['cm_adops@appier.com', 'refund.report.team@appier.com']

            file, image = generate_mail_attachment(info_series, target_inv_level)
            if file is None:
                print('log file not found!')
                continue

            success = send_mail_util(receiver_list, subject, mail_body, image, file)
            if success:
                log_recorder.record(info_series, subject)

    return


def send_campaign_level_mail(data_df, partner, testing=config['testing'], log_only=config['log_only']):

    partner_df = data_df[data_df['partner id'] == partner]
    for cid, campaign_df in partner_df.groupby('cid'):

        print(
            'try to send mail: partner: %s, cid: %s' % (
                partner, cid
            )
        )

        subject, mail_body = generate_campaign_level_mail_text(campaign_df)
        if log_only:
            success = True
        else:
            receiver_list = ['jim.chang@appier.com'] if testing \
                else mailing_list_dict[partner] + ['cm_adops@appier.com', 'refund.report.team@appier.com']

            file = generate_campaign_level_mail_attachment(campaign_df)
            if file is None:
                print('log file not found!')
                continue

            success = send_mail_util(receiver_list, subject, mail_body, None, file)

        if success:
            first_series = campaign_df.iloc[0]
            info_series = {
                'cm': first_series['cm'],
                'partner id': first_series['partner id'],
                'campaign': first_series['campaign'],
                'cid': first_series['cid'],
                'channel id': '--',
                'installs': campaign_df['installs'].sum(),
                'cost': round(campaign_df['cost'].sum(), 2),
                'reason': '--',
                'installs_dedupe': campaign_df['installs_dedupe'].sum(),
                'cpa': '--'
            }
            log_recorder.record(info_series, subject)

    return
