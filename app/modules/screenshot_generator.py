import subprocess
import os

head = '''<html>

<head>
  <style>
    .table-area {
      text-align: center;
      display: block;
      font-family: serif;
      margin: 10px;
    }
    .mail-content {
        display: block;
        width: fit-content;
    }
    .table {
    background-color: rgb(251, 251, 251);
    color: rgb(102, 102, 102);
    font-size: 16px;
    width: max-content;
    border-spacing: 0px;
    padding: 0px;
    border-radius: 5px;
    border-width: 1px;
    border-style: solid;
    border-color: rgb(221, 221, 221);
    border-image: initial;
}
.table-th {
    line-height: 40px;
    text-align: right;
    word-break: break-all;
    min-width: 200px;
    border-right: 1px solid rgb(221, 221, 221);
    border-bottom: 1px solid rgb(221, 221, 221);
    padding: 0px 5px;
}
.table-td {
    line-height: 40px;
    text-align: right;
    border-right: 1px solid rgb(221, 221, 221);
    padding: 0px 5px;
}
  </style>
</head>

<body>
  <div id="table-area" class="table-area">
    <div id="preview-table" class="mail-content">
      <table class="table">
        <thead>
          <tr>
'''
middle = '''
          </tr>
        </thead>
        <tbody>
          <tr>
'''
tail = '''
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>

</html>
'''


def series2html(Se, output, output_columns):
    html_content = head

    for index_to_show, index_of_df in output_columns:
        html_content += f'<th class="table-th">{index_to_show}</th>'

    html_content += middle
    for index_to_show, index_of_df in output_columns:
        if isinstance(Se[index_of_df], float):
            html_content += f'<td class="table-td">{round(Se[index_of_df], 4) }</td>'
        else:
            html_content += f'<td class="table-td">{Se[index_of_df] }</td>'

    html_content += tail
    with open(output, 'w+') as f:
        f.write(html_content)


def series2screenshot(se, output_path, output_columns):
    series2html(se, 'tmp.html', output_columns)
    subprocess.call(
        f'wkhtmltoimage -f jpg  tmp.html {output_path}',
        shell=True
    )
    os.remove('tmp.html')
