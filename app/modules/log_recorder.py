import pandas as pd
from app.constant import config

log_df = None
mail_log_columns = [
    'CM',
    'Partner',
    'Email',
    'Campaign',
    'cid',
    'Low quality App/Site IDs',
    'Low Quality Action',
    'Refund Amount',
    'Reason',
    'Low Quality Action (After deduplication)',
    'CPA',
    'Deliver Time'
]


def init_log():
    global log_df
    log_df = pd.DataFrame(columns=mail_log_columns)


def record(info_series, subject):
    log_df.loc[log_df.shape[0]] = [
        info_series['cm'],
        info_series['partner id'],
        subject,
        info_series['campaign'],
        info_series['cid'],
        info_series['channel id'],
        info_series['installs'],
        info_series['cost'],
        info_series['reason'],
        info_series.get('installs_dedupe', info_series['installs']),
        info_series['cpa'],
        pd.Timestamp.now().ctime()
    ]


def commit_log(file_name):
    file_path = config['mail_log_folder'] + file_name
    log_df.to_csv(file_path)
