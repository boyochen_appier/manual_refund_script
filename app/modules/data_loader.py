import pandas as pd
from app.models.idash_agent import get_info_by_cid_from_idash


def load(data_path):
    info_df = pd.read_csv(data_path)
    info_df.columns = [column.lower() for column in info_df.columns]
    for i in range(info_df.shape[0]):
        oid, campaign, cm = get_info_by_cid_from_idash(info_df.loc[i]['cid'])
        info_df.at[i, 'oid'] = oid
        info_df.at[i, 'campaign'] = campaign
        info_df.at[i, 'cm'] = cm
    info_df = info_df.drop_duplicates(['cid', 'channel id'])
    info_df.fillna('--', inplace=True)
    if 'installs_dedupe' not in info_df:
        info_df['installs_dedupe'] = info_df['installs']
    return info_df
