from xlsxwriter.workbook import Workbook
import pandas as pd


def combine_data_to_xlsx(info_series, log_csv_path, output_filename, output_columns):
    click_logs = pd.read_csv(log_csv_path, index_col=False).fillna('')
    workbook = Workbook(output_filename)
    # sheet1
    sheet1 = workbook.add_worksheet()
    for i, index_pair in enumerate(output_columns):
        index_to_show, index_of_df = index_pair
        sheet1.write(0, i, index_to_show)
        sheet1.write(1, i, info_series[index_of_df])
    # sheet2
    sheet2 = workbook.add_worksheet()
    for i, column_name in enumerate(click_logs):
        sheet2.write(0, i, column_name)
        for j, value in enumerate(click_logs[column_name]):
            sheet2.write(j + 1, i, value)
    workbook.close()


def campaign_level_xlsx(campaign_df, log_csv_paths, output_filename, output_columns):
    workbook = Workbook(output_filename)

    # create main sheet
    main_sheet = workbook.add_worksheet()
    for i, index_pair in enumerate(output_columns):
        index_to_show, index_of_df = index_pair
        main_sheet.write(0, i, index_to_show)

    log_sheet = None
    n_line = 0
    # iter through all inv
    for i, info_series in campaign_df.reset_index().iterrows():
        for j, index_pair in enumerate(output_columns):
            index_to_show, index_of_df = index_pair
            main_sheet.write(i + 1, j, info_series[index_of_df])

        inv = info_series['channel id']
        click_logs = pd.read_csv(log_csv_paths[inv], index_col=False).fillna('')

        if not log_sheet:
            log_sheet = workbook.add_worksheet()
            for i, column_name in enumerate(click_logs):
                log_sheet.write(0, i, column_name)

        for i, column in click_logs.reset_index().iterrows():
            n_line += 1
            for j, value in enumerate(column):
                log_sheet.write(n_line, j, value)

    workbook.close()
