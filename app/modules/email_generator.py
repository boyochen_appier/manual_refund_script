import os

from app.constant import config, OUTPUT_COLUMNS_DICT
from app.modules import xlsx_generator
from app.modules import screenshot_generator


def generate_mail_attachment(info_series, target_inv_level):
    cid = info_series['cid']
    inv = info_series['channel id']
    partner_id = info_series['partner id']
    cache_dir = config['app']['attachment_cache_folder']
    log_csv_path = config['app']['click_log_folder'] + ''.join(x for x in '{}_{}_{}'.format(partner_id, cid, inv) if x.isalnum() or x == '_') + '.csv'
    if not os.path.exists(log_csv_path):
        print("where are you!", log_csv_path)
        return None, None

    output_screenshot_file = os.path.join(cache_dir, 'screenshot', cid, target_inv_level, inv.replace('/', '').replace(' ', '_') + '.png')
    os.makedirs(os.path.join(cache_dir, 'screenshot', cid, target_inv_level), exist_ok=True)
    output_xlsx_file = os.path.join(cache_dir, 'xlsx', cid, target_inv_level, inv.replace('/', '').replace(' ', '_') + '.xlsx')
    os.makedirs(os.path.join(cache_dir, 'xlsx', cid, target_inv_level), exist_ok=True)

    if not os.path.exists(output_xlsx_file):
        xlsx_generator.combine_data_to_xlsx(info_series, log_csv_path, output_xlsx_file, OUTPUT_COLUMNS_DICT[target_inv_level])
    if not os.path.exists(output_screenshot_file):
        screenshot_generator.series2screenshot(info_series, output_screenshot_file, OUTPUT_COLUMNS_DICT[target_inv_level])
    return output_xlsx_file, output_screenshot_file


def generate_mail_text(info_series):
    adgroup_name = info_series['campaign']
    cm_name = info_series['cm']
    cid = info_series['cid']
    partner_id = info_series['partner id']
    install_count = info_series['installs']
    channel_id = info_series['channel id']
    cost = round(float(info_series['cost']), 2)
    refund_duration = config['app']['refund_duration']

    subject = f'[{partner_id} refund] {refund_duration} {adgroup_name} {channel_id}'

    mail_body = f'Hi {partner_id} Team,\n' +\
        f'This email is regarding the campaign traffic quality issue in {adgroup_name} ({cid})\n' +\
        '\n' +\
        f'We received refund request from the client because the quality of the {install_count} installs from {partner_id} during {refund_duration} is low.\n' +\
        """
        Therefore, we would like to ask your help to proceed refund for this case.
        The 1st tab of the attached excel is the performance
        The 2nd tab of the attached excel is the click ID record

        """ +\
        f'Low quality App/Site IDs: {channel_id}\n' +\
        f'Low Quality Action: {install_count}\n' +\
        (f'Low Quality Action (After deduplication): {info_series.installs_dedupe}\n' if 'installs_dedupe' in info_series else '') +\
        f'Refund Amount: {cost} USD\n' +\
        """

        Thank you very much.


        Best regards,
        """ +\
        f'{cm_name}'

    return subject, mail_body


def generate_campaign_level_mail_attachment(campaign_df):
    if campaign_df.shape[0] == 0:
        return None

    cache_dir = config['app']['attachment_cache_folder']
    log_csv_paths = {}

    for i, info_series in campaign_df.iterrows():
        cid = info_series['cid']
        inv = info_series['channel id']
        partner_id = info_series['partner id']
        inv_log_csv_path = config['app']['click_log_folder'] + ''.join(x for x in '{}_{}_{}'.format(partner_id, cid, inv) if x.isalnum() or x == '_') + '.csv'
        if os.path.exists(inv_log_csv_path):
            log_csv_paths[inv] = inv_log_csv_path

    output_xlsx_file = os.path.join(cache_dir, 'xlsx', cid, 'campaign_level.xlsx')
    os.makedirs(os.path.join(cache_dir, 'xlsx', cid), exist_ok=True)

    if not os.path.exists(output_xlsx_file):
        xlsx_generator.campaign_level_xlsx(campaign_df, log_csv_paths, output_xlsx_file, OUTPUT_COLUMNS_DICT['basic'])
    return output_xlsx_file


def generate_campaign_level_mail_text(campaign_df):
    if campaign_df.shape[0] == 0:
        return None, None

    adgroup_name = campaign_df['campaign'].iloc[0]
    cm_name = campaign_df['cm'].iloc[0]
    cid = campaign_df['cid'].iloc[0]
    partner_id = campaign_df['partner id'].iloc[0]
    install_count = campaign_df['installs_dedupe'].sum()
    cost = round(float(campaign_df['cost'].sum()), 2)
    refund_duration = config['app']['refund_duration']

    subject = f'[{partner_id} refund] [campaign level] {refund_duration} {adgroup_name}'

    mail_body = f'Hi {partner_id} Team,\n' +\
        f'This email is regarding the campaign traffic quality issue in {adgroup_name} ({cid})\n' +\
        '\n' +\
        f'We received refund request from the client because the quality of the {install_count} installs from {partner_id} during {refund_duration} is low.\n' +\
        """
        Therefore, we would like to ask your help to proceed refund for this case.
        The 1st tab of the attached excel is the performance
        The 2nd tab of the attached excel is the click ID record

        """ +\
        f'Low Quality Action: {install_count}\n' +\
        f'Refund Amount: {cost} USD\n' +\
        """

        Thank you very much.


        Best regards,
        """ +\
        f'{cm_name}'

    return subject, mail_body
