import requests
import json


class IDash:
    def __init__(self):
        self.session = None
        self.url = 'https://idash.appier.net:9935'
        self.username = 'robot_crawler2'
        self.password = 'ALeeSSEq9DI6F1yF'
        self.login()

    def _arg_process(self, args, kargs):
        if len(args) >= 1:
            args = list(args)
            args[0] = self.url + args[0]
            args = tuple(args)

        if 'url' in kargs:
            kargs['url'] = f'{self.url}{kargs["url"]}'
        return args, kargs

    def get(self, *args, **kargs):
        args, kargs = self._arg_process(args, kargs)
        response = self.session.get(*args, **kargs)
        while not self.check_status(response):
            response = self.session.get(*args, **kargs)
        return response

    def post(self, *args, **kargs):
        args, kargs = self._arg_process(args, kargs)
        response = self.session.post(*args, **kargs)
        while not self.check_status(response):
            response = self.session.post(*args, **kargs)
        return response

    def login(self):
        payload = {
            'username': self.username,
            'password': self.password
        }

        self.session = requests.Session()
        response = self.session.post(f'{self.url}/login', payload, 'POST')
        if response is not None and response.status_code == 200:
            print('idash login sucess')
        else:
            print('idash login failed')

    def check_status(self, response):
        if response.status_code == 401:
            self.login()
            return False
        return True

    def rejudge_need_info(self, oid):
        result = {
            'oid': oid,
            'comid': None,
            'region': None,
            'cids': None,
            'ssps': None
        }

        campaign = self.campaign(oid, fields=['comid', 'campaigns'])
        result['cids'] = campaign.get('campaigns', [])
        result['comid'] = campaign.get('comid')

        if result['cids'] and result['comid']:
            company = self.company(result['comid'], fields=['company_region'])
            adgroups = self.adgroups(
                result['cids'],
                fields=['campaign_other_type']
            )

            result['region'] = company.get('campany_region')
            result['ssps'] = list(set([
                adgroup.get('campaign_other_type') for adgroup in adgroups
                if adgroup.get('campaign_other_type')
            ]))

        return result

    def company(self, comid, fields=[]):
        data = {
            'comid': comid
        }
        if fields:
            data['fields'] = json.dumps(fields)

        response = self.get('/read/company', params=data)
        result = response.json()
        return result[0] if result else {}

    def campaign(self, oid, fields=[]):
        data = {'oid': oid}
        if fields:
            data['fields'] = json.dumps(fields)

        response = self.get('/read/order', params=data)
        result = response.json()
        return result[0] if result else {}

    def adgroup(self, cid, fields=[]):
        data = {
            'cid': cid
        }
        if fields:
            data['fields'] = json.dumps(fields)

        response = self.get('/read/campaign', params=data)
        result = response.json()
        return result[0] if result else {}

    def adgroups(self, cids, fields=[]):
        data = {
            'cids': json.dumps(cids)
        }
        if fields:
            data['fields'] = json.dumps(fields)

        response = self.post('/read/campaign', data=data)
        result = response.json()
        return result if result else {}

    def events(self, oid, fields=[]):
        data = {
            'oid': oid
        }
        if fields:
            data['fields'] = json.dumps(fields)
        response = self.get('/read/trackeventgroup', params=data)
        result = response.json()
        return result if result else []

    def blacklist(self, cid, reject_type, source='all'):
        if source == 'all':
            data = {
                'formatted': False,
            }
        else:
            data = {
                'formatted': True,
                'sources': json.dumps([source])
            }

        response = self.get(f'/v2/get/blacklist_whitelist/{cid}', params=data)
        result = response.json()
        if result.get('success'):
            return result.get('result', {}).get(f'reject_{reject_type}', [])
        return []

    def save_blacklist(self, cid, reject_type, addlist, removelist):
        data = {
            'publishers_to_add': addlist,
            'publishers_to_remove': removelist,
            'reject_publisher_type': f'reject_{reject_type}'
        }

        response = self.post(f'/save/cm_blacklist/{cid}', json=data)
        result = response.json()
        return result

    def recover_fraud(self, recover_fraud_list):
        data = {
            '_jsonStr': json.dumps({
                'data': recover_fraud_list
            })
        }
        response = self.post('/fraud/v2/control/recover', data=data)
        if response.status_code == 204:
            return True
        return False


idash = IDash()
